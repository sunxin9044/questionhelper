//
//  Menu.m
//  Helper
//
//  Created by Xin Sun on 15/8/16.
//  Copyright (c) 2015年 Xin Sun. All rights reserved.
//

#import "Menu.h"
#import <QuartzCore/QuartzCore.h>

@interface Menu()
@property (nonatomic, strong) NSMutableArray* btns;

@property (nonatomic) CGFloat px;
@property (nonatomic) CGFloat py;

@property (nonatomic, strong) NSIndexPath* path;

@property (assign, nonatomic, getter=isNeedAdjustButtonFrame) BOOL needAdjustButtonFrame;
@end

@implementation Menu

- (NSMutableArray *)btns {
    if (!_btns) {
        _btns = [[NSMutableArray alloc] init];
    }
    
    return _btns;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    self.hidden = YES;
    [self initBtns];
    
    return self;
}

#define FINISH 1
#define NEED_CHECK 2
- (void)initBtns {
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(1, 1, 30, 30)];
    btn1.backgroundColor = [UIColor colorWithRed:77/255.0 green:188/255.0 blue:30/255.0 alpha:1.0];
    [btn1 setTitle:@"完成" forState:UIControlStateNormal];
    [btn1 setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn1.layer.cornerRadius = 15;
    btn1.transform = CGAffineTransformMakeScale(0, 0);
    btn1.alpha = 0;
    btn1.tag = FINISH;
    [btn1 addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn1];
    [self.btns addObject:btn1];
    
    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(32, 1, 30, 30)];
    btn2.backgroundColor = [UIColor colorWithRed:255/255.0 green:228/255.0 blue:66/255.0 alpha:1.0];
    btn2.alpha = 0;
    btn2.transform = CGAffineTransformMakeScale(0, 0);
    btn1.tag = NEED_CHECK;
    [btn2 addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn2.layer.cornerRadius = 15;
    [btn2 setTitle:@"复习" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [self addSubview:btn2];
    [self.btns addObject:btn2];
}

- (void)btnClicked:(UIButton *)btn{
    [_delegate btnClicked:btn.tag withTarget:self.path];
    self.needAdjustButtonFrame = NO;
    [self.btns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        [UIView animateWithDuration:0.2 delay:0.05 * idx options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
            btn.transform = transform;
            btn.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
        }];
    }];
}

- (BOOL)checkPoints:(CGPoint)points {
    return _px == points.x && _py == points.y;
}

- (void)setClickPoints:(CGPoint)points andCell:(NSIndexPath*) path {
    _px = points.x;
    _py = points.y;
    _path = path;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.isNeedAdjustButtonFrame) {
        int startX = self.px + 20 - 32;
        if (startX < 0) {
            startX = 1;
        } else if (startX + 64 > [[UIScreen mainScreen] bounds].size.width) {
            startX = [[UIScreen mainScreen] bounds].size.width - 64 - 1;
        }
        int startY = self.py - 20;
        self.frame = CGRectMake(startX, startY, self.frame.size.width, self.frame.size.height);
        
        self.needAdjustButtonFrame = NO;
    }
}

- (void)inacivate {
    if (!self.hidden) {
        [self.btns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
            [UIView animateWithDuration:0.2 delay:0.05 * idx options:UIViewAnimationOptionCurveEaseOut animations:^{
                CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
                btn.transform = transform;
                btn.alpha = 0;
            } completion:^(BOOL finished) {
                self.hidden = YES;
            }];
        }];
    }
}

- (void)activate:(BOOL)samePoints {
    if (self.hidden) {
        self.needAdjustButtonFrame = YES;
        self.hidden = NO;
        [self.btns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
            [UIView animateWithDuration:0.5 delay:0.05 * idx usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
                btn.transform = CGAffineTransformMakeScale(1, 1);
                btn.alpha = 1;
            } completion:nil];
        }];
    } else {
        self.needAdjustButtonFrame = NO;
        [self.btns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
            [UIView animateWithDuration:0.2 delay:0.05 * idx options:UIViewAnimationOptionCurveEaseOut animations:^{
                CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
                btn.transform = transform;
                btn.alpha = 0;
            } completion:^(BOOL finished) {
                if (samePoints) {
                    self.hidden = YES;
                } else {
                    self.needAdjustButtonFrame = YES;
                    [self.btns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
                        [UIView animateWithDuration:0.5 delay:0.05 * idx usingSpringWithDamping:1 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
                            btn.transform = CGAffineTransformMakeScale(1, 1);
                            btn.alpha = 1;
                        } completion:nil];
                    }];
                }
            }];
        }];
    }
}

@end
