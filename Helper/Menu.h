//
//  Menu.h
//  Helper
//
//  Created by Xin Sun on 15/8/16.
//  Copyright (c) 2015年 Xin Sun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumberCell.h"
@class Menu;
@protocol MenuEventDelegate <NSObject>
@required
// 点击功能按钮
- (void)btnClicked:(NSInteger)btnTag withTarget:(NSIndexPath *)cell;
@end

@interface Menu:UIView

@property (weak, nonatomic) id<MenuEventDelegate> delegate;

- (void)activate:(BOOL)samePoints;
- (void)setClickPoints:(CGPoint)points andCell:(NSIndexPath *)path;
- (BOOL)checkPoints:(CGPoint)points;
- (void)inacivate;

@end