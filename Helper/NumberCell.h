//
//  UICollectionViewCell_NumberCell.h
//  Helper
//
//  Created by Xin Sun on 15/8/16.
//  Copyright (c) 2015年 Xin Sun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumberCell:UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
