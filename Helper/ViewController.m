//
//  ViewController.m
//  Helper
//
//  Created by Xin Sun on 15/8/16.
//  Copyright (c) 2015年 Xin Sun. All rights reserved.
//

#import "ViewController.h"
#import "NumberCell.h"
#import "Menu.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, MenuEventDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView* collectionView;
@property (nonatomic, strong) NSMutableArray* arr;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray* picked;
@property (nonatomic, strong) NSMutableArray* checked;
@property (nonatomic, strong) Menu* menu;
@end

@implementation ViewController
- (NSMutableArray *) arr {
    if (!_arr) {
        _arr = [[NSMutableArray alloc] init];
    }
    
    return _arr;
}

- (NSMutableArray *) picked {
    if (!_picked) {
        _picked = [[NSMutableArray alloc] init];
    }
    
    return _picked;
}

- (NSMutableArray *) checked {
    if (!_checked) {
        _checked = [[NSMutableArray alloc] init];
    }
    
    return _checked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    int input[] = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,31,32,33,34,35,36,37,38,39,40,41,42,43,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,127,128,129,131,132,133,134,135,136,137,138,139,141,142,143,144,145,147,148,149,150,151,152,153,154,155,160,162,164,165,166,168,169,171,172,173,179,187,189,190,191,198,199,200,201,202,203,204,205,206,207,208,209,210,211,213,215,216,217,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,257,258,260,263,264,268,273,274,275,278};
    for (int i=0; i<214; i++) {
        [self.arr addObject:[NSString stringWithFormat:@"%d", input[i]]];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"picked"]!=nil) {
        self.picked = [[NSUserDefaults standardUserDefaults] objectForKey:@"picked"];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"checked"]!=nil) {
        self.checked = [[NSUserDefaults standardUserDefaults] objectForKey:@"checked"];
    }
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%lu(%.1f%%)",self.picked.count + self.checked.count, (unsigned long)self.arr.count, ((float)(self.picked.count + self.checked.count)/(float)self.arr.count)*100];
    
    self.menu = [[Menu alloc] initWithFrame:CGRectMake(0, 0, 64, 34)];
    self.menu.delegate = self;
    [self.view addSubview:self.menu];
    
    UITapGestureRecognizer *doubleTapFolderGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(processDoubleTap:)];
    [doubleTapFolderGesture setNumberOfTapsRequired:2];
    [doubleTapFolderGesture setNumberOfTouchesRequired:1];
    doubleTapFolderGesture.delaysTouchesBegan = YES;
    [self.view addGestureRecognizer:doubleTapFolderGesture];
}

- (void) processDoubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
        if (indexPath)
        {
            if ([self.checked containsObject:[self.arr objectAtIndex:indexPath.row]]) {
                [self.checked removeObject:[self.arr objectAtIndex:indexPath.row]];
            }
            if ([self.picked containsObject:[self.arr objectAtIndex:indexPath.row]]) {
                [self.picked removeObject:[self.arr objectAtIndex:indexPath.row]];
            }
            [[NSUserDefaults standardUserDefaults] setObject:self.checked forKey:@"checked"];
            [[NSUserDefaults standardUserDefaults] setObject:self.picked forKey:@"picked"];
            NumberCell *cell = (NumberCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            cell.backgroundColor = [UIColor whiteColor];
            self.titleLabel.text = [NSString stringWithFormat:@"%ld/%lu(%.1f%%)",self.picked.count + self.checked.count, (unsigned long)self.arr.count, ((float)(self.picked.count + self.checked.count)/(float)self.arr.count)*100];
        }
    }
}

#define FINISH 2
- (void)btnClicked:(NSInteger)btnTag withTarget:(NSIndexPath *)path
{
    NumberCell *cell = (NumberCell *)[self.collectionView cellForItemAtIndexPath:path];
    if (btnTag == FINISH) {
        cell.backgroundColor = [UIColor colorWithRed:77/255.0 green:188/255.0 blue:30/255.0 alpha:1.0];
        if ([self.checked containsObject:[self.arr objectAtIndex:path.row]]) {
            [self.checked removeObject:[self.arr objectAtIndex:path.row]];
        }
        if ([self.picked containsObject:[self.arr objectAtIndex:path.row]]) {
            [self.picked removeObject:[self.arr objectAtIndex:path.row]];
        }
        [self.picked addObject:[self.arr objectAtIndex:path.row]];
        [[NSUserDefaults standardUserDefaults] setObject:self.checked forKey:@"checked"];
        [[NSUserDefaults standardUserDefaults] setObject:self.picked forKey:@"picked"];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:228/255.0 blue:66/255.0 alpha:1.0];
        if ([self.checked containsObject:[self.arr objectAtIndex:path.row]]) {
            [self.checked removeObject:[self.arr objectAtIndex:path.row]];
        }
        if ([self.picked containsObject:[self.arr objectAtIndex:path.row]]) {
            [self.picked removeObject:[self.arr objectAtIndex:path.row]];
        }
        [self.checked addObject:[self.arr objectAtIndex:path.row]];
        [[NSUserDefaults standardUserDefaults] setObject:self.checked forKey:@"checked"];
        [[NSUserDefaults standardUserDefaults] setObject:self.picked forKey:@"picked"];
    }
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%lu(%.1f%%)",self.picked.count + self.checked.count, (unsigned long)self.arr.count, ((float)(self.picked.count + self.checked.count)/(float)self.arr.count)*100];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 214;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NumberCell *cell = (NumberCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"number" forIndexPath:indexPath];
    
    if ([self.picked containsObject:[self.arr objectAtIndex:indexPath.row]]) {
        cell.backgroundColor = [UIColor colorWithRed:77/255.0 green:188/255.0 blue:30/255.0 alpha:1.0];
    } else if ([self.checked containsObject:[self.arr objectAtIndex:indexPath.row]]) {
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:228/255.0 blue:66/255.0 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.title.text = [self.arr objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGPoint pos = attributes.frame.origin;
    pos = [collectionView convertPoint:pos toView:self.view];
    
    bool status = [self.menu checkPoints:pos];
    if (!status) {
        [self.menu setClickPoints:pos andCell:indexPath];
    }
    [self.menu activate:status];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.menu inacivate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
